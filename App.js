/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaView,StyleSheet,ScrollView, View,Text,StatusBar,ImageBackground} from 'react-native';
import {Provider} from 'react-redux';
import store from './src/redux/store';

import AppStack from './src/navigator/AppStack';
import LoginScreen from './src/screens/LoginScreen';
import HomePageScreen from './src/screens/HomePageScreen';
import RegisterScreen from './src/screens/RegisterScreen';

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <AppStack/>
    </Provider>
  );
};

export default App;
