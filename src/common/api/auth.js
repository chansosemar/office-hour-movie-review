import axios from 'axios';

export function apiLogin(dataLogin) {
  return axios({
    method: 'POST',
    url: 'http://ec2-52-77-224-102.ap-southeast-1.compute.amazonaws.com/login',
    data: dataLogin,
  });
}
