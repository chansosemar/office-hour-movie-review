import axios from 'axios';

export function apiFetchProfileDetail(id, headers) {
  return axios({
    method: 'GET',
    url: 'http://ec2-52-77-224-102.ap-southeast-1.compute.amazonaws.com/users',
    headers,
  });
}
