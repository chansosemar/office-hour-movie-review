import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome';

import ProfileScreen from '../screens/ProfileScreen';
import HomePageScreen from '../screens/HomePageScreen';
import ReviewScreen from '../screens/ReviewScreen';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
  return (
      <Tab.Navigator
      initialRouteName={'Homepage'}
      tabBarOptions={{
        activeTintColor: '#e09f3e',
        inactiveTintColor: '#fff',
        showLabel:false,
        style: {
          backgroundColor: '#222',
          height:60,
      },
      }}
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;

          if (route.name === 'Review') {
            iconName = 'comments';
          } else if (route.name === 'Homepage') {
            iconName ='home'
          }else if (route.name === 'Profile') {
            iconName ='user'
          }
          return <Icon name={iconName} size={size} color={color} />;
        },
      })}>
        <Tab.Screen name="Review" component={ReviewScreen} />
        <Tab.Screen name="Homepage" component={HomePageScreen} />
        <Tab.Screen name="Profile" component={ProfileScreen} />
      </Tab.Navigator>
  );
};

export default MainNavigator;
