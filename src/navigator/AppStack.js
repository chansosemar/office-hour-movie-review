import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {connect} from 'react-redux';
import {logoutAction} from '../redux/action/authAction';

import MainNavigator from './MainNavigator';
import LoginScreen from '../screens/LoginScreen';
import RegisterScreen from '../screens/RegisterScreen';



const Stack = createStackNavigator();

const AppStack = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {props.statusLogin ? (
          <Stack.Screen
            name="MainNavigator"
            component={MainNavigator}
            options={{ headerShown:false}}
          />
        ) : (
          <>
          <Stack.Screen
              options={{headerShown: false}}
              name="LoginScreen"
              component={LoginScreen}
            />
            <Stack.Screen
              options={{headerShown: false}}
              name="RegisterScreen"
              component={RegisterScreen}
            />
         </>
        )}
      </Stack.Navigator>

    </NavigationContainer>
  );
};

const mapStateToProps = (state) => ({
  statusLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({
  processLogout: () => dispatch(logoutAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);
