import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  Modal,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles/ProfileStyle';
import {connect} from 'react-redux';
import {apiFetchProfileDetail} from '../common/api/profile';

function ProfileScreen(props) {
  // const [listUsers, setListUsers] = useState([]);
  //
  // useEffect(() => {
  //   const fetchData = async () => {
  //     const result = await apiFetchProfileDetail();
  //     setListUsers(result.data);
  //   };
  //
  //   fetchData();
  // }, []);


  return (
    <View style={styles.window}>
      <View style={styles.container}>
        <View style={styles.heading}>
          <Text style={styles.headingPage}>YOUR PROFILE |</Text>
          <Text style={styles.subHeadingPage}> {props.profile.username.toUpperCase()}</Text>
        </View>
        <View style={styles.contProfile}>
          <View style={styles.contLogo}>
            <Image source={require('../assets/logo.png')} style={styles.logo} />
          </View>
          <View style={styles.profile}>
            <View style={styles.contText}>
              <Text style={styles.dataProfile}>NAME </Text>
              <Text style={styles.dataInput}>{props.profile.full_name.toUpperCase()}</Text>
            </View>
            <View style={styles.contText}>
              <Text style={styles.dataProfile}>USERNAME </Text>
              <Text style={styles.dataInput}>{props.profile.username.toUpperCase()}</Text>
            </View>

            <View style={styles.contText}>
              <Text style={styles.dataProfile}>EMAIL </Text>
              <Text style={styles.dataInput}>{props.profile.email.toUpperCase()}</Text>
            </View>
          </View>
          <View style={styles.centerPos}>
            <TouchableOpacity
              onPress={() => submit()}
              style={styles.mainButton}>
              <Text style={styles.buttonText}>EDIT PROFILE</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => submit()}
              style={styles.warnButton}>
              <Text style={styles.buttonText}>LOG OUT</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
}

const mapStateToProps= (state) =>({
  profile : state.profile.data[0]

})

const mapDispatchToProps= (dispatch) => ({

})

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
