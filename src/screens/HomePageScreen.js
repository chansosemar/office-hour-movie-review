import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  Button,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Modal from 'react-native-modal';
import styles from './styles/HomePageStyles';
import {apiDataMovies} from '../common/api/API';

export default function HomePageScreen(props) {
  const [listGenre, setListGenre] = useState([]);
  const [listMovie, setListMovie] = useState([]);
  const [activeIndex, setActiveIndex] = useState('All');
  const [isModalVisible, setModalVisible] = useState(false);
  const [dataFilter, setDataFilter] = useState([])
  const [dataModal, setDataModal] = useState([])
  const [selectedMovie, setSelectedMovie] = useState({})



const test = (items) => {
  setActiveIndex(items);
  setDataFilter(listMovie.filter((item) => {
    return item.genre == items ;
  }))
  console.info('data filter :', dataFilter)
}



const getUnique = (array) => {
        var uniqueArray = [];
        for(var value of array){
            if(uniqueArray.indexOf(value) === -1){
                uniqueArray.push(value);
            }
        }
        return uniqueArray;
    }

  const toggleModal = (items) => {
    setModalVisible(!isModalVisible);
    setSelectedMovie(items)
    console.info('data selectedMovie :', selectedMovie)
  };

  const toggleModalClose = () => {
    setModalVisible(isModalVisible == false);
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await apiDataMovies();
      setListMovie(result.data);
      setDataFilter(result.data);
      const temp = [];
      result.data.map((movie) => temp.push(movie.genre));
      temp.filter((a, b) => temp.indexOf(a) === b);
      const unique = getUnique(temp);
      setListGenre(unique);
      console.log(unique);
    };
    fetchData();
  }, []);


  return (
    <View style={styles.window} key="index">
      <View style={styles.container}>
        <View style={styles.contSearch}>
          <Icon
            style={styles.searchIcon}
            name="search"
            size={20}
            color={'#c4c4c4'}
          />
          <TextInput
            placeholder="Search Movies or Actor"
            placeholderTextColor={'#c4c4c4'}
            style={styles.formTextInput}
          />
        </View>
        <View style={styles.heading}>
          <Text style={styles.headingPage}>GENRE |</Text>
          <Text style={styles.subHeadingPage}> SWIPE FOR MORE</Text>
        </View>
        <View>
          <ScrollView
            style={styles.contGenre}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            {listGenre.map((items, index) => (
              <TouchableOpacity
                onPress={() => {
                   test(items);
                }}
                style={activeIndex === items ? styles.btnActive : styles.btn}
                key={index}>
                <Text style={styles.sliderText}>{items}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
        <View style={styles.heading}>
          <Text style={styles.smallHeadingPage}>
            {activeIndex.toUpperCase()} |
          </Text>
          <Text style={styles.smallSubHeadingPage}> MOVIES GENRE >></Text>
        </View>

        <View style={styles.contScroll}>
          <ScrollView
            style={styles.scroll}
            horizontal={true}
            pagingEnabled={true}
            overScrollMode={'never'}>
            {dataFilter.map((items, index) => (
              <TouchableOpacity
              style={styles.contCard}
              key={index}
              onPress={() => toggleModal(items)}
              >
                <Image source={{uri: items.image}} style={styles.cardImage} />
                <View style={styles.mainContent}>
                  <Text style={styles.contentTitle} numberOfLines={1}>
                    {items.title.toUpperCase()}
                  </Text>
                  <Text style={styles.subContentTitle}>
                    {items.genre.toUpperCase()}
                  </Text>
                </View>
                <View style={styles.subContent}>
                  <Text style={styles.contentTitle}>4/5 |</Text>
                  <Text style={styles.subContentTitle}> {items.date}</Text>
                </View>
                <View style={styles.contSynopsys}>
                  <Text style={styles.synopsisText} numberOfLines={4}>
                    {items.synopsis}
                  </Text>
                </View>

                <View style={styles.cardIcon}>
                  <Icon name="comments" size={20} color={'#000'}>
                    <Text style={styles.smallText}> {items.number}</Text>
                  </Icon>
                  <Icon name="share-alt" size={20} color={'#000'} />
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
          <Modal isVisible={isModalVisible}>
             <View
             style={styles.contCard}
             >
               <Image source={{uri: selectedMovie.image }} style={styles.cardImage} />
               <View style={styles.mainContent}>
                 <Text style={styles.contentTitle} numberOfLines={1}>
                   {selectedMovie.title}
                 </Text>
                 <Text style={styles.subContentTitle}>
                   {selectedMovie.genre}
                 </Text>
               </View>
               <View style={styles.subContent}>
                 <Text style={styles.contentTitle}>4/5 |</Text>
                 <Text style={styles.subContentTitle}> {selectedMovie.date}</Text>
               </View>
               <View style={styles.contSynopsys}>
                 <Text style={styles.synopsisText} numberOfLines={4}>
                   {selectedMovie.synopsis}
                 </Text>
               </View>

               <View style={styles.cardIcon}>
                 <Icon name="comments" size={20} color={'#000'}>
                   <Text style={styles.smallText}> 123</Text>
                 </Icon>
                 <Icon name="share-alt" size={20} color={'#000'} />
               </View>
               <Button title="Hide modal" onPress={() => toggleModalClose()}/>
             </View>
           </Modal>

        </View>
      </View>
    </View>
  );
}
