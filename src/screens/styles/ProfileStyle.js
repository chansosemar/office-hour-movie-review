import {StyleSheet, Dimensions} from 'react-native';
const contWidth = Dimensions.get('window').width * 0.9;
const contHeight = Dimensions.get('window').height;
const posHeight = Dimensions.get('window').height * 0.1;

export default StyleSheet.create({
  window: {
    backgroundColor: '#222',
    alignItems: 'center',
    height: contHeight,
  },
  container: {
    width: contWidth,
  },
  heading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  headingPage: {
    color: '#fff',
    fontSize: 30,
    paddingTop: 10,
    fontFamily: 'Roboto-Bold',
  },
  subHeadingPage: {
    color: '#fff',
    fontSize: 30,
    paddingTop: 10,
    fontFamily: 'Roboto-Thin',
  },
  contLogo: {
    alignItems: 'center',
    marginBottom: 30,
  },
  logo: {
    width: 100,
    height: 100,
  },
  contProfile: {
    top: posHeight,
    alignItems: 'center',
  },
  profile: {
    alignItems: 'flex-start',
    width: '70%',
  },
  contText: {
    display: 'flex',
    flexDirection: 'row',
  },
  dataProfile: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto-Bold',
  },
  dataInput: {
    color: '#fff',
    fontSize: 20,
    fontFamily: 'Roboto-Thin',
  },
  centerPos: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
  },
  mainButton: {
    padding: 10,
    backgroundColor: '#335c67',
    width: 150,
    height: 30,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
  },
  warnButton: {
    padding: 10,
    backgroundColor: 'red',
    width: 150,
    height: 30,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
  },
  buttonText: {
    color: '#fff',
    fontSize: 15,
    fontFamily: 'Roboto-Bold',
  },
});
