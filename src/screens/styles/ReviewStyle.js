import {StyleSheet, Dimensions} from 'react-native';
const contWidth = Dimensions.get('window').width * 0.9;
const contHeight = Dimensions.get('window').height;
const heightScroll = Dimensions.get('window').height * 0.8;

export default StyleSheet.create({
  window: {
    backgroundColor: '#222',
    alignItems: 'center',
    height: contHeight,
  },
  container: {
    width: contWidth,
  },
  heading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  headingPage: {
    color: '#fff',
    fontSize: 30,
    paddingTop: 10,
    fontFamily: 'Roboto-Bold',
  },
  subHeadingPage: {
    color: '#fff',
    fontSize: 30,
    paddingTop: 10,
    fontFamily: 'Roboto-Thin',
  },
  contScroll: {
    alignItems: 'center',
    width: contWidth,
  },
  scroll: {
    height: heightScroll,
  },
  contCard: {
    backgroundColor: '#c4c4c4',
    borderRadius: 10,
    margin: 10,
    width: 350,
  },
  contReview: {
    display: 'flex',
    flexDirection: 'row',
    padding: 10,
  },
  contContent: {
    flex: 2,
    alignItems: 'flex-start',
    justifyContent: 'center',
    padding: 10,
  },
  cardImage: {
    height: 30,
    width: 30,
    borderRadius: 50,
    padding: 50,
    resizeMode: 'cover',
  },
  smallText: {
    fontSize: 10,
  },
  mainContent: {
    display: 'flex',
    flexDirection: 'row',
  },
  subContent: {
    display: 'flex',
    flexDirection: 'row',
  },
  contentTitle: {
    color: '#222',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
  },
  subContentTitle: {
    color: '#222',
    fontFamily: 'Roboto-Light',
    fontSize: 20,
  },
  subContentReview: {
    color: '#222',
    fontWeight: '100',
    fontSize: 15,
    textAlign: 'justify',
  },
  actorContent: {
    alignItems: 'center',
  },
  actorName: {
    color: '#222',
    fontWeight: '100',
    fontSize: 20,
  },
  cardIcon: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
});
