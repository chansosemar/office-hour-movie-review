import {StyleSheet, Dimensions} from 'react-native';

const contWidth = Dimensions.get('window').width * 0.9;
const contHeight = Dimensions.get('window').height;
const posHeight = Dimensions.get('window').height * 0.15;

export default StyleSheet.create({
  window: {
    backgroundColor: '#222',
    alignItems: 'center',
    height: contHeight,
  },
  container: {
    width: contWidth,
    margin: 10,
  },
  contLogo: {
    alignItems: 'center',
    marginBottom: 50,
  },
  logo: {
    width: 100,
    height: 100,
  },
  contInput: {
    position: 'relative',
    top: posHeight,
  },
  warnText: {
    color: '#9e2a2b',
    padding: 10,
    fontSize: 10,
  },
  formInput: {
    backgroundColor: 'rgba(255,255 ,255 ,0.2 )',
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    color: '#fff',
  },
  centerPos: {
    alignItems: 'center',
    margin: 10,
  },
  mainButton: {
    padding: 10,
    backgroundColor: '#335c67',
    width: 200,
    height: 50,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
  },
  smallText: {
    color: '#fff',
    fontSize: 15,
  },
  smallTextButton: {
    color: 'orange',
    fontSize: 15,
    marginTop: 5,
  },
});
