import {StyleSheet, Dimensions} from 'react-native';
const contWidth = Dimensions.get('window').width * 0.9;
const contHeight = Dimensions.get('window').height + 40;

export default StyleSheet.create({
  cont: {
    padding: 20,
  },
  window: {
    backgroundColor: '#222',
    alignItems: 'center',
    height: contHeight,
  },
  container: {
    width: contWidth,
  },
  contSearch: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255 ,255 ,0.2 )',
    borderRadius: 10,
    width: contWidth,
    marginTop: 20,
  },
  searchIcon: {
    left: 20,
  },
  formTextInput: {
    width: contWidth,
    color: '#424242',
    borderRadius: 50,
    left: 30,
    color: '#fff',
    fontSize: 15,
  },
  heading: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  headingPage: {
    color: '#fff',
    fontSize: 30,
    fontFamily: 'Roboto-Black',
    paddingTop: 10,
  },
  subHeadingPage: {
    color: '#fff',
    fontSize: 30,
    fontFamily: 'Roboto-Thin',
    paddingTop: 10,
  },
  smallHeadingPage: {
    color: '#fff',
    fontSize: 25,
    fontFamily: 'Roboto-Black',
    paddingTop: 10,
  },
  smallSubHeadingPage: {
    color: '#fff',
    fontSize: 25,
    fontFamily: 'Roboto-Thin',
    paddingTop: 10,
  },
  contGenre: {
    marginTop: 10,
  },
  btn: {
    padding: 10,
    width: 100,
    height: 50,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    backgroundColor: '#335c67',
  },
  btnActive: {
    padding: 10,
    width: 100,
    height: 50,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 5,
    backgroundColor: '#e09f3e',
  },
  sliderText: {
    color: '#fff',
    fontWeight: 'bold',
  },
  contScroll: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    height: contHeight,
  },
  scroll: {
    marginBottom: 350,
  },
  contCard: {
    backgroundColor: '#c4c4c4',
    borderRadius: 10,
    margin: 10,
    width: 350,
  },
  contTitle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerGenre: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: 10,
  },
  subHeaderText: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'right',
    marginTop: 15,
  },

  cardImage: {
    height: 120,
    width: 350,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },

  mainContent: {
    alignItems: 'center',
    padding: 10,
  },
  subContent: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: -10,
    justifyContent: 'center',
  },
  contentTitle: {
    color: '#222',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
  },
  subContentTitle: {
    color: '#222',
    fontFamily: 'Roboto-Light',
    fontSize: 20,
  },
  contSynopsis: {
    alignItems: 'center',
  },
  synopsisText: {
    color: '#222',
    fontWeight: '100',
    fontSize: 15,
    textAlign: 'justify',
    padding: 10,
  },
  cardIcon: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingBottom: 20,
    top:20
  },
  smallText: {
    fontSize: 15,
  },
});
