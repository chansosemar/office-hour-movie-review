import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  Modal,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {apiDataMovies} from '../common/api/API';
import styles from './styles/ReviewStyle';

export default function ReviewScreen() {
  const [listGenre, setListGenre] = useState([]);
  const [listMovie, setListMovie] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await apiDataMovies();
      setListMovie(result.data);
      const temp = [];
      result.data.map((movie) => temp.push(movie.genre));
      temp.filter((a, b) => temp.indexOf(a) === b);
      setListGenre(temp);
      console.log(temp);
    };

    fetchData();
  }, []);

    return (
      <View style={styles.window}>
        <View style={styles.container}>
          <View style={styles.heading}>
            <Text style={styles.headingPage}>YOUR REVIEW |</Text>
            <Text style={styles.subHeadingPage}> CHANDRA</Text>
          </View>
          <View style={styles.contScroll}>
            <ScrollView style={styles.scroll}>
              {listMovie.map((items, index) => (
                <View style={styles.contCard} key={index}>
                  <View style={styles.contReview}>
                    <View>
                      <Image
                        source={{uri: items.image}}
                        style={styles.cardImage}
                      />
                    </View>
                    <View style={styles.contContent}>
                      <Text style={styles.contentTitle} numberOfLines={2}>{items.title.toUpperCase()}</Text>
                      <View style={styles.mainContent}>
                        <Text style={styles.subContentTitle}> 4/5 </Text>
                        <Icon name="edit" size={20} color={'#e09f3e'} />
                      </View>
                      <View style={styles.subContent}>
                        <Text style={styles.subContentReview}>
                          Wiih sangat bagus sekali !! tidak bosan bosannya
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              ))}
            </ScrollView>
          </View>
        </View>
      </View>
    );
  }
