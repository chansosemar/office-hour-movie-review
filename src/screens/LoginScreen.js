import SplashScreen from 'react-native-splash-screen';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles/LoginStyle';
import RegisterScreen from './RegisterScreen';
import {useNavigation} from '@react-navigation/native';
import {connect} from 'react-redux';
import {loginAction} from '../redux/action/authAction'

function LoginScreen(props) {
  const [username, setUsername] = useState('septianmp1');
  const [password, setPassword] = useState('septianmp1');
  const [message, setMessage] = useState(null);
  const navigation = useNavigation();

  useEffect(() => {
    return SplashScreen.hide();
  });

  const submit = () => {
    if (!username) {
      setMessage('Username Must be Field !!');
    } else if (!password) {
      setMessage('Password Must be Field !!');
    } else {
      props.processLogin({username, password});
    }
  };

  return (


      <KeyboardAvoidingView style={styles.window} >
        <View style={styles.container}>
          <View style={styles.contInput}>
          <View style={styles.contLogo}>
            <Image source={require('../assets/logo.png')} style={styles.logo} />
          </View>
          <Text style={styles.warnText}>{message}</Text>
          <TextInput
            onChangeText={(text) => setUsername(text)}
            value={username}
            placeholder="username"
            placeholderTextColor="#888"
            style={styles.formInput}
          />
          <TextInput
            onChangeText={(text) => setPassword(text)}
            value={password}
            placeholder="password"
            placeholderTextColor="#888"
            style={styles.formInput}
            secureTextEntry
          />
          <View style={styles.rightPos}>
            <TouchableOpacity>
              <Text style={styles.warnText}>Forgot Password ?</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.centerPos}>
            {props.isLoading ? (
              <TouchableOpacity
                onPress={() => console.info('disabled')}
                style={styles.mainButton}>
                <ActivityIndicator size="large" color="#333333" />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => submit()}
                style={styles.mainButton}>
                <Text style={styles.buttonText}>LOGIN</Text>
              </TouchableOpacity>
            )}
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('RegisterScreen')}>
            <View style={styles.centerPos}>
              <Text style={styles.smallTextButton}>
                Don't have an account ? REGISTER
              </Text>
            </View>
          </TouchableOpacity>
          <View style={styles.centerPos}>
            <Text style={styles.smallText}>or LOGIN with</Text>
          </View>
          <View style={styles.social}>
            <TouchableOpacity style={styles.socialButton}>
              <Text>
                <Icon name="facebook" size={30} color={'#fff'} />
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.socialButton}>
              <Text>
                <Icon name="google" size={30} color={'#fff'} />
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.socialButton}>
              <Text>
                <Icon name="linkedin" size={30} color={'#fff'} />
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </KeyboardAvoidingView>


  );
}



const mapStateToProps= (state) =>({
  isLoading: state.auth.isLoading
})

const mapDispatchToProps= (dispatch) => ({
  processLogin: (data) => dispatch(loginAction(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
