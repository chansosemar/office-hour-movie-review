import {takeLatest, put} from 'redux-saga/effects';
import {apiLogin} from '../../common/api/auth';
import {ToastAndroid} from 'react-native';
import {removeToken,saveAccountId, saveToken} from '../../common/function/auth';
import {LOGIN, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT} from '../action/actionTypes';
import {getProfileDetail} from '../action/profileAction';
import AsyncStorage from '@react-native-async-storage/async-storage';

function* login(action) {
  try {

    const resLogin = yield apiLogin(action.payload);

    if (resLogin && resLogin.data) {

      yield saveToken(resLogin.data.token);
      yield saveAccountId(resLogin.data.id);

      yield put({type: LOGIN_SUCCESS});

      ToastAndroid.showWithGravity(
        `LOGIN SUKSES LET'S GIVE SOME STAR`,
        ToastAndroid.SHORT,
        ToastAndroid.CENTER,
      );
      yield put({type : 'GET_PROFILE'});
    } else {
      // show alert
      ToastAndroid.showWithGravity(
        'Login gagal',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      yield put({type: LOGIN_FAILED});
    }
  } catch (e) {
    console.info('e', e);
    // show alert
    ToastAndroid.showWithGravity(
      'Gagal login',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
    yield put({type: LOGIN_FAILED});
  }
}

function* logout(action) {
  try {
    yield removeToken();
  } catch (e) {
    ToastAndroid.showWithGravity(
      'Gagal logout',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );
  }
}

function* authSaga() {
  yield takeLatest(LOGIN, login);
  yield takeLatest(LOGOUT, logout);
}

export default authSaga;
