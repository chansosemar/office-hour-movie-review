import {takeLatest, put} from 'redux-saga/effects';
import {apiFetchProfileDetail} from '../../common/api/profile';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILED,
} from '../action/actionTypes';

function* getProfileDetail() {
  try {
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    const resProfile = yield apiFetchProfileDetail(accountId, headers);
    console.info('ini resprofile', resProfile.data)
    yield put({type: GET_PROFILE_SUCCESS, payload: resProfile.data});
    console.log('berhasil ambil data profile');
  } catch (e) {

    ToastAndroid.showWithGravity(
      'Gagal mengambil data profil',
      ToastAndroid.SHORT,
      ToastAndroid.CENTER,
    );

    yield put({type: GET_PROFILE_FAILED});
  }
}

function* profileSaga() {
  yield takeLatest(GET_PROFILE, getProfileDetail);
}

export default profileSaga;
