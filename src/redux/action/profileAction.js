import {GET_PROFILE} from './actionTypes';

export const getProfileDetail = () => {
  return {type: GET_PROFILE};
};
