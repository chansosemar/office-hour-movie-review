import {GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILED, LOGOUT} from './reducerTypes';

// initial state = nilai awal data profile yang ada di store
const initialState = {
  isLoading: false,
  data : {id: null,
  username: null,
  email: null,
  password: null,
  full_name: null,
  url_image: null}
};

const profile = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFILE: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case GET_PROFILE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        data : action.payload

      };
    }
    case GET_PROFILE_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case LOGOUT: {
      return {
        isLoading: false,
        id: null,
        username: null,
        email: null,
        password: null,
        full_name: null,
        url_image: null
      };
    }
    default:
      return state;
  }
};

export default profile;
